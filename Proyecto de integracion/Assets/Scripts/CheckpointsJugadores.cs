using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointsJugadores : MonoBehaviour
{
    public Transform checkpoint; 

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Limits"))
        {
            transform.position = checkpoint.position;
        }
    }
}
