using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Pj2Control : MonoBehaviour
{
    public float velocidad;
    public float fuerzaDeSalto;
    public float saltoMaximo;
    public LayerMask Piso;

    private Rigidbody2D rb;
    private BoxCollider2D boxCollider;
    private bool mirandoDerecha = true;
    private float saltosRestantes;
    private Animator animator;
    private Vector2 inputMovimiento;
    private PlayerInput2 inputActions;

    private Vector3 escalaInicial;
    private float direccionMovimiento = 1f; 

    private void Awake()
    {
        inputActions = new PlayerInput2();
        inputActions.Player2.Move.performed += ctx => inputMovimiento = ctx.ReadValue<Vector2>();
        inputActions.Player2.Move.canceled += ctx => inputMovimiento = Vector2.zero;
        inputActions.Player2.Jump.performed += ctx => Salto();
        inputActions.Player2.SizeDecreased.started += ctx => CambiarTamanio(0.5f); // Reducir tamaño
        inputActions.Player2.SizeIncreased.started += ctx => VolverAlTamañoOriginal();
        inputActions.Player2.Escape.started += ctx => Salir();// Volver al tamaño original
    }

    private void OnEnable()
    {
        inputActions.Player2.Enable();
    }

    private void OnDisable()
    {
        inputActions.Player2.Disable();
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        saltosRestantes = saltoMaximo;
        animator = GetComponent<Animator>();
        escalaInicial = transform.localScale;
    }

    private void Update()
    {
        Movimiento();
    }
    
    private void Salir()
    {
        SceneManager.LoadScene(0);
    }

    private void Movimiento()
    {
        float movimientoHorizontal = inputMovimiento.x * direccionMovimiento * velocidad;

        rb.velocity = new Vector2(movimientoHorizontal, rb.velocity.y);

        animator.SetBool("EstaCaminando2", Mathf.Abs(inputMovimiento.x) > 0.1f);

        if (inputMovimiento.x > 0 && !mirandoDerecha)
        {
            
            Girar();
        }
        else if (inputMovimiento.x < 0 && mirandoDerecha)
        {
       
            Girar();
        }
    }

    private bool EstaEnSuelo()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0f, Vector2.down, 0.2f, Piso);
        return raycastHit.collider != null;
    }

    private void Salto()
    {
        if (EstaEnSuelo())
        {
            saltosRestantes = saltoMaximo;
        }

        if (saltosRestantes > 0)
        {
            saltosRestantes--;
            rb.velocity = new Vector2(rb.velocity.x, 0f);
            rb.AddForce(Vector2.up * fuerzaDeSalto, ForceMode2D.Impulse);
            animator.SetTrigger("Salta2");
        }
    }

    private void Girar()
    {
        mirandoDerecha = !mirandoDerecha;
        transform.Rotate(0f, 180f, 0f); 
    }

    private void CambiarTamanio(float factor)
    {
     
        transform.localScale = new Vector3(escalaInicial.x * factor, escalaInicial.y * factor, escalaInicial.z); ;
    }

    private void VolverAlTamañoOriginal()
    {
       
        transform.localScale = escalaInicial;
    }
}
