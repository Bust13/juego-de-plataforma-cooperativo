using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TriggerNivel : MonoBehaviour
{
    private CamaraTransitionManager cameraTransitionManager;
    private HashSet<GameObject> playersInTrigger = new HashSet<GameObject>();
    public GameObject cambioDeNivel;
    public GameObject limites;

    void Start()
    {
        cameraTransitionManager = FindObjectOfType<CamaraTransitionManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playersInTrigger.Add(collision.gameObject);


            if (playersInTrigger.Count >= 2)
            {
                cameraTransitionManager.MoverProxPosicion();
                cambioDeNivel.SetActive(false);
                limites.SetActive(true);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playersInTrigger.Remove(collision.gameObject);
        }
    }
}
