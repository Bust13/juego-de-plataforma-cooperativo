using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerCheckPoint : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            CheckpointsJugadores playerCheckpoint = collision.GetComponent<CheckpointsJugadores>();
            if (playerCheckpoint != null)
            {
                playerCheckpoint.checkpoint = transform;
            }
        }
    }
}
